BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `visite` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`idPrecedant`	INTEGER,
	`idMedecin`	CHAR,
	`etablissement`	VARCHAR,
	`date`	DATETIME,
	`diagnostic`	VARCHAR,
	`traitement`	VARCHAR,
	`resume`	VARCHAR,
	`notes`	VARCHAR
);
INSERT INTO `visite` VALUES (1,0,'Albert','St-Luc','2001-01-01','le patient a mal a la tete','tylenol 3 fois par jour','Examen bref','Il avait simplement mal a la tete');
INSERT INTO `visite` VALUES (2,1,'Albert','St-Luc','2001-01-02','le patient a encore mal a la tete','tylenol 4 fois par jour','Examen bref','Il a definitivement un probleme dans la tete');
INSERT INTO `visite` VALUES (3,0,'Albert','Notre-Dame','2001-01-02','le patient a mal au genoux','repos 3 jours','Examen bref','Aucune');
INSERT INTO `visite` VALUES (8,2,'Albert','mal au pied',3893,'mal au pied','repos trois jours','tout cest bien passe','Aucune');
CREATE TABLE IF NOT EXISTS `modification` (
	`idModif`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`idPrecedant`	INTEGER,
	`idDossier`	INTEGER,
	`prenom`	VARCHAR,
	`nom`	VARCHAR,
	`dateNaiss`	DATETIME,
	`genre`	INTEGER,
	`parents`	VARCHAR,
	`villeNaiss`	VARCHAR,
	`adresse`	VARCHAR,
	`telephone`	VARCHAR,
	`courriel`	VARCHAR,
	`noAssMal`	INTEGER,
	`idAntecedent`	INTEGER,
	`idVisite`	INTEGER
);
INSERT INTO `modification` VALUES (8,0,1,'gino','chouinard','3900-02-01',2,'gino, gina','Longueil','123 Longueil st','450-434-4334','gino.chouinard@hotmail.com',123321123,20,19);
INSERT INTO `modification` VALUES (9,8,1,'gina','chouinard','3900-02-01',2,'gino, gina','Longueil','123 Longueil st','450-434-4334','gino.chouinard@hotmail.com',123321123,20,19);
INSERT INTO `modification` VALUES (10,9,1,'gina','chouinard','3900-02-01',2,'gino, gina','Longueil','123 Longueil st','450-434-4334','gino.chouinard@hotmail.com',123321123,20,19);
INSERT INTO `modification` VALUES (11,10,1,'GINO','CHOUINARD','3900-02-01',1,'GINO SR. , GINA','Longueil','123 Longueil St','514-514-5114','gino.chouinard@hotmail.com',123321123,20,19);
CREATE TABLE IF NOT EXISTS `dossier` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`prenom`	VARCHAR,
	`nom`	VARCHAR,
	`dateNaiss`	DATETIME,
	`genre`	INTEGER,
	`parents`	VARCHAR,
	`villeNaiss`	VARCHAR,
	`adresse`	VARCHAR,
	`telephone`	VARCHAR,
	`courriel`	VARCHAR,
	`noAssMal`	INTEGER,
	`idAntecedent`	INTEGER,
	`idVisite`	INTEGER,
	`idMod`	INTEGER DEFAULT 0
);
INSERT INTO `dossier` VALUES (1,'GINO','CHOUINARD','5800-03-01',1,'GINO SR. , GINA','Longueil','123 Longueil St','514-514-5114','gino.chouinard@hotmail.com',123321123,20,19,11);
INSERT INTO `dossier` VALUES (2,'eric','lapointe','2002-01-01',1,'eriko, erika','Laval','321 Laval st','450-979-9779','eric.lapointe@hotmail.com',987789987,3,3,0);
CREATE TABLE IF NOT EXISTS `antecedent` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`idPrecedant`	INTEGER DEFAULT 0,
	`diagnostic`	TEXT,
	`traitement`	VARCHAR,
	`idMedecin`	VARCHAR,
	`debutMaladie`	DATETIME,
	`finMaladie`	DATETIME
);
INSERT INTO `antecedent` VALUES (1,0,'Le patient avait mal à la tête','Tylenol 10X200mg 2 comprimés par jours','Albert','2000-01-01','2000-01-03');
INSERT INTO `antecedent` VALUES (2,1,'Le patient avait encore mal à la tête','Tylenol 25X200mg 2 comprimés par jours','Albert','2000-01-04','2000-01-07');
INSERT INTO `antecedent` VALUES (3,0,'Le patient avait mal au genoux','repos pendant 2 jours','Isaac','2000-02-04','2000-02-06');
INSERT INTO `antecedent` VALUES (17,2,'diagnostic','traitement','medecin','3900-04-03','3900-04-03');
INSERT INTO `antecedent` VALUES (18,17,'diagnostic','traitement','medecin','3900-04-03','3900-04-03');
INSERT INTO `antecedent` VALUES (19,18,'mal au pied','repos trois jours','albert','3900-04-03','3900-04-03');
INSERT INTO `antecedent` VALUES (20,19,'mal au pied','repos trois jours','albert','3900-04-03','3900-04-03');
INSERT INTO `antecedent` VALUES (21,20,'mal au pied','repos trois jours','albert','3900-04-03','3900-04-03');
INSERT INTO `antecedent` VALUES (22,21,'mal au pied','repos trois jours','albert','3900-04-03','3900-04-03');
COMMIT;
