BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `authentificationPersonnel` (
	`id`	VARCHAR NOT NULL,
	`password`	VARCHAR,
	PRIMARY KEY(`id`)
);
INSERT INTO `authentificationPersonnel` VALUES ('Pythagore','deSamos');
INSERT INTO `authentificationPersonnel` VALUES ('Andrew','Wiles');
INSERT INTO `authentificationPersonnel` VALUES ('Wilhelm','Leibniz');
INSERT INTO `authentificationPersonnel` VALUES ('Alan','Turing');
INSERT INTO `authentificationPersonnel` VALUES ('René','Descartes');
CREATE TABLE IF NOT EXISTS `authentificationPatient` (
	`id`	VARCHAR NOT NULL,
	`password`	VARCHAR,
	PRIMARY KEY(`id`)
);
INSERT INTO `authentificationPatient` VALUES ('Sebastian','Bach');
INSERT INTO `authentificationPatient` VALUES ('Xian','Xinghai');
INSERT INTO `authentificationPatient` VALUES ('Bob','Marley');
INSERT INTO `authentificationPatient` VALUES ('Miles','Davis');
INSERT INTO `authentificationPatient` VALUES ('Lucianno','Pavarotti');
CREATE TABLE IF NOT EXISTS `authentificationMedecin` (
	`id`	VARCHAR NOT NULL,
	`password`	VARCHAR,
	PRIMARY KEY(`id`)
);
INSERT INTO `authentificationMedecin` VALUES ('Albert','Einstein');
INSERT INTO `authentificationMedecin` VALUES ('Nikola','Tesla
Tesla');
INSERT INTO `authentificationMedecin` VALUES ('Leonardo','Davinci');
INSERT INTO `authentificationMedecin` VALUES ('Isaac','Newton');
INSERT INTO `authentificationMedecin` VALUES ('Thomas','Jefferson');
COMMIT;
