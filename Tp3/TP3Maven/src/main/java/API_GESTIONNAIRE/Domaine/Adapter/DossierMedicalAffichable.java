/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package API_GESTIONNAIRE.Domaine.Adapter;

/**
 *
 * @author cedric
 */
public interface DossierMedicalAffichable {
        String obtenirNom();
        String obtenirPrenom();
        String obtenirDateNaissance();
        String obtenirGenre();
        String obtenirParents();
        String obtenirVille();
        String obtenirNumeroTelephone();
        String obtenirAdresse();
        String obtenirCourriel();
        String obtenirNumeroAssuranceMaladie();
        String obtenirAntecedents();
        String obtenirVisites();
    
}
