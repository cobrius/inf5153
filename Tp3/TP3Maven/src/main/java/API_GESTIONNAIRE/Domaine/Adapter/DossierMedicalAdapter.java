/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package API_GESTIONNAIRE.Domaine.Adapter;

import API_GESTIONNAIRE.Domaine.Antecedent;
import API_GESTIONNAIRE.Domaine.DossierMedical;
import API_GESTIONNAIRE.Domaine.Patient;
import API_GESTIONNAIRE.Domaine.Visite;
import java.util.ArrayList;

/**
 *
 * @author cedric
 */
public class DossierMedicalAdapter extends DossierMedical implements DossierMedicalAffichable {

    public DossierMedicalAdapter(int numeroAssuranceMaladie) throws Exception {
        super(numeroAssuranceMaladie);
    }

    @Override
    public String obtenirNom() {
        Patient patient = this.getPatient();
        return patient.getNom();
    }

    @Override
    public String obtenirPrenom() {
        Patient patient = this.getPatient();
        return patient.getPrenom();
    }

    @Override
    public String obtenirGenre() {
        Patient patient = this.getPatient();
        return "Pas encore implemente";
    }

    @Override
    public String obtenirParents() {
        Patient patient = this.getPatient();
        return patient.getParentsConnus();
    }

    @Override
    public String obtenirVille() {
        Patient patient = this.getPatient();
        return patient.getVilleNaissance();
    }

    @Override
    public String obtenirNumeroTelephone() {
        Patient patient = this.getPatient();
        return patient.getCoordonnees().getNumeroTelephone();
    }

    @Override
    public String obtenirAdresse() {
        Patient patient = this.getPatient();
        return patient.getCoordonnees().getAdresse();
    }

    @Override
    public String obtenirCourriel() {
        Patient patient = this.getPatient();
        return "Pas encore implementer l'obtention du courriel ";
    }

    @Override
    public String obtenirNumeroAssuranceMaladie() {
        Patient patient = this.getPatient();
        return Integer.toString(patient.getNumeroAssuranceMaladie());
    }

    @Override
    public String obtenirDateNaissance() {
        Patient patient = this.getPatient();
        // System.out.println(patient.getDateNaissance().toString());
        return patient.getDateNaissance().toString().substring(4, 11) + patient.getDateNaissance().toString().substring(24, 28);
    }

    @Override
    public String obtenirAntecedents() {
       ArrayList<Antecedent> antecedents= this.getAntecedents();
       String retour = "------- Antécédents médicaux -------- \n\n \n";
       for(Antecedent antecedent :antecedents){
          retour += antecedent.getDebutMaladie().toString().substring(4, 11)  ;
          retour+= antecedent.getDebutMaladie().toString().substring(24, 28) + "-";
          retour += antecedent.getFinMaladie().toString().substring(4, 11)  ;
          retour+= antecedent.getFinMaladie().toString().substring(24, 28) + "\n";
          retour+="    "+ antecedent.getDiagnostic().toString() + "\n";
          retour+="Traitement Reçu:" + antecedent.getTraitement().toString()+ "\n";
          retour+= "Par " + antecedent.getIdMedecin() +"\n \n";
       }
       return retour;
    }

    @Override
    public String obtenirVisites() {
         ArrayList<Visite> visites= this.getVisites();
       String retour = "------- Visites-------- \n\n \n";
       for(Visite visite :visites){
          retour +="Etablissement : " + visite.getIdEtablissement() + " \n" ;
          retour+= "Medecin vu : " + visite.getIdMedecin()+ " ";
          retour += "le " + visite.getDate().toString().substring(4, 11) + visite.getDate().toString().substring(24, 28)  + "\n";
          if (visite.getDiagnostic()!= null){
            retour+= "Diagnostic: " + visite.getDiagnostic().toString()+ "\n";
          }
          if (visite.getTraitement() != null){
            retour+="Traitement: "+ visite.getTraitement().toString() + "\n";
          }
          retour+="Résumé: " + visite.getResume()+ "\n";
          retour+= "Notes:  " + visite.getNotes() +"\n \n";
       }
       return retour;
    }
}


