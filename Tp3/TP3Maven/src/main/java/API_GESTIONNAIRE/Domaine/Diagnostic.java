package API_GESTIONNAIRE.Domaine;

/**
 * Détails d'un diagnostic
 *
 * @author Mathieu
 */
public class Diagnostic {

    private final String details;

    public Diagnostic(String details) {
        this.details = details;
    }

    public String getDetails() {
        return this.details;
    }

    @Override
    public String toString() {
        return this.details.toString();
    }
}
