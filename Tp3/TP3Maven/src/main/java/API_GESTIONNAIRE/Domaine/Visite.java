package API_GESTIONNAIRE.Domaine;

import java.util.Date;

/**
 * Détails d'une visite
 *
 * @author Mathieu
 */
public class Visite {

    public static VisiteBuilder VisiteBuilder() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private final int id;
    private final int idPrecedant;
    private final String idMedecin;
    private final String idEtablissement;
    private final Date date;
    private final Diagnostic diagnostic;
    private final Traitement traitement;
    private final String resume;
    private final String notes;

    /**
     * Builder de la classe visite
     */
    public static class VisiteBuilder {

        private int id = -1;
        private int idPrec = -1;
        private String idMedecin = "";
        private String idEtablissement = "";
        private Date date = null;
        private Diagnostic diagnostic = null;
        private Traitement traitement = null;
        private String resume = "";
        private String notes = "";

        public VisiteBuilder() {
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setIdPrecedant(int idPrec) {
            this.idPrec = idPrec;
        }

        public void setIdMedecin(String idMedecin) {
            this.idMedecin = idMedecin;
        }

        public void setIdEtablissement(String idEtablissement) {
            this.idEtablissement = idEtablissement;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public void setDiagnostic(Diagnostic diagnostic) {
            this.diagnostic = diagnostic;
        }

        public void setTraitement(Traitement traitement) {
            this.traitement = traitement;
        }

        public void setResume(String resume) {
            this.resume = resume;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public Visite getResult() {
            return new Visite(this);
        }
    }

    public Visite(VisiteBuilder builder) {
        this.id = builder.id;
        this.idPrecedant = builder.idPrec;
        this.idMedecin = builder.idMedecin;
        this.idEtablissement = builder.idEtablissement;
        this.date = builder.date;
        this.diagnostic = builder.diagnostic;
        this.traitement = builder.traitement;
        this.resume = builder.resume;
        this.notes = builder.notes;
    }

    public int getId() {
        return this.id;
    }

    public int getIdPrecedant() {
        return this.idPrecedant;
    }

    public String getIdMedecin() {
        return this.idMedecin;
    }

    public String getIdEtablissement() {
        return this.idEtablissement;
    }

    public Date getDate() {
        return this.date;
    }

    public String getResume() {
        return this.resume;
    }

    public String getNotes() {
        return this.notes;
    }

    public Traitement getTraitement() {
        return this.traitement;
    }

    public Diagnostic getDiagnostic() {
        return this.diagnostic;
    }

    /**
     * Valider une visite
     *
     * @throws Exception
     */
    public void ValiderDonnees() throws Exception {
        if (this.idMedecin.equals("")) {
            throw new Exception("Vous devez saisir l'identifiant du médecin.");
        } else if (this.idEtablissement.equals("")) {
            throw new Exception("Vous devez saisir l'établissement.");
        } else if (this.date == null) {
            throw new Exception("Vous devez saisir la date de la visite.");
        } else if (this.diagnostic.toString().equals("")) {
            throw new Exception("Vous devez saisir le diagnostic du patient.");
        }/* else if (this.resume.equals("")) {
            throw new Exception("Vous devez saisir le resumé de la visite.");
        }*/
    }

    @Override
    public String toString() {
        return "id=" + this.id
                + ", idPrecedant=" + this.idPrecedant
                + ", idMedecin=" + this.idMedecin
                + ", idEtablissement" + this.idEtablissement
                + ", date=" + this.date
                + ", diagnostic=" + this.diagnostic.toString()
                + ", traitement=" + this.traitement.toString()
                + ", resume=" + this.resume
                + ", notes=" + this.notes;
    }
}
