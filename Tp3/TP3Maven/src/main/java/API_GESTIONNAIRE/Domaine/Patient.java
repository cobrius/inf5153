package API_GESTIONNAIRE.Domaine;

import java.util.Date;

/**
 * Détails d'un patient
 *
 * @author Mathieu
 */
public class Patient {

    private final int numeroAssuranceMaladie;
    private final int versionPrecedente;
    private final String nom;
    private final String prenom;
    private final Date dateNaissance;
    private final int genre;
    private final String villeNaissance;
    private final String parentsConnus;
    private final Coordonnees coordonnees;

    /**
     * Builder
     */
    public static class PatientBuilder {

        private int numeroAssuranceMaladie = -1;
        private int versionPrecedente = -1;
        private String nom = "";
        private String prenom = "";
        private Date dateNaissance = null;
        private int genre = -1;
        private String villeNaissance = "";
        private String parentsConnus = "";
        private Coordonnees coordonnees = null;

        public PatientBuilder() {
        }

        public void setNumeroAssuranceMaladie(int numeroAssuranceMaladie) {
            this.numeroAssuranceMaladie = numeroAssuranceMaladie;
        }

        public void setGenre(int genre) {
            this.genre = genre;
        }

        public void setVersionPrecedente(int versionPrecedente) {
            this.versionPrecedente = versionPrecedente;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public void setPrenom(String prenom) {
            this.prenom = prenom;
        }

        public void setDateNaissance(Date dateNaissance) {
            this.dateNaissance = dateNaissance;
        }

        public void setVilleNaissance(String villeNaissance) {
            this.villeNaissance = villeNaissance;
        }

        public void setParentsConnus(String parentsConnus) {
            this.parentsConnus = parentsConnus;
        }

        public void setCoordonnees(Coordonnees coordonnees) {
            this.coordonnees = coordonnees;
        }

        public Patient getResult() {
            return new Patient(this);
        }
    }

    public Patient(PatientBuilder builder) {
        this.numeroAssuranceMaladie = builder.numeroAssuranceMaladie;
        this.versionPrecedente = builder.versionPrecedente;
        this.nom = builder.nom;
        this.prenom = builder.prenom;
        this.dateNaissance = builder.dateNaissance;
        this.genre = builder.genre;
        this.villeNaissance = builder.villeNaissance;
        this.parentsConnus = builder.parentsConnus;
        this.coordonnees = builder.coordonnees;
    }

    public int getNumeroAssuranceMaladie() {
        return this.numeroAssuranceMaladie;
    }

    public int getVersionPrecedente() {
        return this.versionPrecedente;
    }

    public String getNom() {
        return this.nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public int getGenre() {
        return this.genre;
    }

    public Date getDateNaissance() {
        return this.dateNaissance;
    }

    public String getVilleNaissance() {
        return this.villeNaissance;
    }

    public String getParentsConnus() {
        return this.parentsConnus;
    }

    public Coordonnees getCoordonnees() {
        return this.coordonnees;
    }

    /**
     * Valider les données d'un patient
     *
     * @throws Exception
     */
    public void ValiderDonnees() throws Exception {
        if (this.numeroAssuranceMaladie == -1) {
            throw new Exception("Vous devez saisir le numéro d'assurance maladie du patient.");
        } else if (this.nom.equals("")) {
            throw new Exception("Vous devez saisir le nom du patient.");
        } else if (this.prenom.equals("")) {
            throw new Exception("Vous devez saisir le prénom du patient.");
        } else if (this.dateNaissance == null) {
            throw new Exception("Vous devez saisir la date de naissance du patient.");
        } else if (this.villeNaissance.equals("")) {
            throw new Exception("Vous devez saisir la ville de naissance du patient.");
        }

        this.coordonnees.ValiderDonnees();
    }

    @Override
    public String toString() {
        return "numeroAssuranceMaladie=" + this.numeroAssuranceMaladie
                + ", versionPrecedente=" + this.versionPrecedente
                + ", nom=" + this.nom
                + ", prenom=" + this.prenom
                + ", dateNaissance=" + this.dateNaissance
                + ", genre=" + this.genre
                + ", villeNaissance=" + this.villeNaissance
                + ", parentsConnus=" + this.parentsConnus
                + ", coordonnees=" + this.coordonnees;
    }
}
