package API_GESTIONNAIRE.Domaine;

/**
 * Détails d'un traitement
 *
 * @author Mathieu
 */
public class Traitement {

    private final String details;

    public Traitement(String details) {
        this.details = details;
    }

    public String getDetails() {
        return this.details;
    }

    @Override
    public String toString() {
        return this.details.toString();
    }
}
