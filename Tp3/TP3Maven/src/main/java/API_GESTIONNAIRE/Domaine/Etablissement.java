package API_GESTIONNAIRE.Domaine;

/**
 * Détails d'un établissement de la RAMQ
 *
 * @author Cédric, Charles-Olivier & Mathieu
 */
public class Etablissement {

    private final int id;
    private final String nom;

    public Etablissement(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public int getId() {
        return this.id;
    }

    public String getNom() {
        return this.nom;
    }

    @Override
    public String toString() {
        return "id=" + this.id
                + ", nom=" + this.nom;
    }
}
