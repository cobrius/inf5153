package API_GESTIONNAIRE.Domaine;

import API_RAMQ.ServicesTechniques.ObtenirDossier;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Le dossier médical d'un patient
 *
 * @author Mathieu
 */
public class DossierMedical {

    private int id;
    private Patient patient;
    private ArrayList<Antecedent> antecedents;
    private ArrayList<Visite> visites;

    public DossierMedical(int noAssuranceMaladie) throws Exception {
        String donnees = ObtenirDossier.obtenirDossier(noAssuranceMaladie);

        if (donnees.isEmpty()) {
            throw new Exception("Le numéro d'assurance maladie est invalide.");
        }
        parserDonnees(donnees);
    }

    public int getId() {
        return this.id;
    }

    public Patient getPatient() {
        return this.patient;
    }

    public ArrayList<Antecedent> getAntecedents() {
        return this.antecedents;
    }

    public ArrayList<Visite> getVisites() {
        return this.visites;
    }

    public void ajouterVisite(Visite visite) {
        this.visites.add(visite);
    }

    public void ajouterAntecedent(Antecedent antecedent) {
        this.antecedents.add(antecedent);
    }

    /**
     * Tranforme un string en DossierMedical
     *
     * @param donnees
     */
    private void parserDonnees(String donnees) throws Exception {
        ChargerPatient(donnees);
        ChargerAntecedents(donnees);
        ChargerVisites(donnees);
    }

    /**
     * Charger les données du patient(nom, prénom...)
     *
     * @param donnees
     */
    private void ChargerPatient(String donnees) throws Exception {
        StringTokenizer st = new StringTokenizer(donnees, "$");
        SimpleDateFormat tmpDate = new SimpleDateFormat("yyyy-MM-dd");
        Patient.PatientBuilder builder = new Patient.PatientBuilder();
        Coordonnees.Builder builderCoord = new Coordonnees.Builder();

        st.nextToken();
        this.id = Integer.parseInt(st.nextToken());
        st.nextToken();
        builder.setPrenom(st.nextToken());
        st.nextToken();
        builder.setNom(st.nextToken());
        st.nextToken();
        builder.setDateNaissance(tmpDate.parse(st.nextToken()));
        st.nextToken();
        builder.setGenre(Integer.parseInt(st.nextToken()));
        st.nextToken();
        builder.setParentsConnus(st.nextToken());
        st.nextToken();
        builder.setVilleNaissance(st.nextToken());
        st.nextToken();

        // Coordonnees
        builderCoord.setAdresse(st.nextToken());
        st.nextToken();
        builderCoord.setNumeroTelephone(st.nextToken());
        st.nextToken();
        builderCoord.setAdresseCourriel(st.nextToken());
        builder.setCoordonnees(new Coordonnees(builderCoord));
        st.nextToken();
        builder.setNumeroAssuranceMaladie(Integer.parseInt(st.nextToken()));
        this.patient = new Patient(builder);
    }

    /**
     * Charger les antécédents du patient
     *
     * @param donnees
     */
    private void ChargerAntecedents(String donnees) throws Exception {
        SimpleDateFormat tmpDate = new SimpleDateFormat("yyyy-MM-dd");
        String donneesAnte = donnees.substring(0, donnees.lastIndexOf("$"));
        StringTokenizer st = new StringTokenizer(donneesAnte, "*");
        this.antecedents = new ArrayList<>();

        while (st.hasMoreTokens()) {
            Antecedent.AntecedentBuilder builder = new Antecedent.AntecedentBuilder();

            st.nextToken();
            builder.setId(Integer.parseInt(st.nextToken()));
            st.nextToken();
            builder.setIdPrecedant(Integer.parseInt(st.nextToken()));
            st.nextToken();
            builder.setDiagnostic(new Diagnostic(st.nextToken()));
            st.nextToken();
            builder.setTraitement(new Traitement(st.nextToken()));
            st.nextToken();
            builder.setIdMedecin(st.nextToken());
            st.nextToken();
            builder.setDebutMaladie(tmpDate.parse(st.nextToken()));
            st.nextToken();
            builder.setFinMaladie(tmpDate.parse(st.nextToken()));
            this.antecedents.add(new Antecedent(builder));
        }
    }

    /**
     * Charger les visites du patients
     *
     * @param donnees
     */
    private void ChargerVisites(String donnees) throws Exception {
        SimpleDateFormat tmpDate = new SimpleDateFormat("yyyy-MM-dd");
        String donneesVisite = donnees.substring(donnees.lastIndexOf("$"), donnees.length() - 1);
        StringTokenizer st = new StringTokenizer(donneesVisite, "/");
        visites = new ArrayList<>();

        while (st.hasMoreTokens()) {
            Visite.VisiteBuilder builder = new Visite.VisiteBuilder();

            st.nextToken();
            builder.setId(Integer.parseInt(st.nextToken()));
            st.nextToken();
            builder.setIdPrecedant(Integer.parseInt(st.nextToken()));
            st.nextToken();
            builder.setIdMedecin(st.nextToken());
            st.nextToken();
            builder.setIdEtablissement(st.nextToken());
            st.nextToken();
            builder.setDate(tmpDate.parse(st.nextToken()));
            st.nextToken();
            builder.setDiagnostic(new Diagnostic(st.nextToken()));
            st.nextToken();
            builder.setTraitement(new Traitement(st.nextToken()));
            st.nextToken();
            builder.setResume(st.nextToken());
            st.nextToken();
            builder.setNotes(st.nextToken());
            visites.add(new Visite(builder));
        }
    }
}
