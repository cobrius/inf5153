package API_GESTIONNAIRE.Domaine;

/**
 * Détails d'un médecin
 *
 * @author Mathieu
 */
public class Medecin implements ProfessionnelSante {

    private String id;
    private String nom;
    private String prenom;

    public Medecin(String id, String nom, String prenom) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public String getPrenom() {
        return this.prenom;
    }

    @Override
    public String toString() {
        return "id=" + this.id
                + ", nom=" + this.nom
                + ", prenom=" + this.prenom;
    }
}
