package API_GESTIONNAIRE.Domaine;

import API_RAMQ.ServicesTechniques.Modification;
import API_RAMQ.ServicesTechniques.ValiderAcces;
import API_RAMQ.ServicesTechniques.ObtenirDossier;
import API_GESTIONNAIRE.Domaine.Adapter.DossierMedicalAdapter;
import API_GESTIONNAIRE.Domaine.Adapter.DossierMedicalAffichable;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Gestionnaire
 *
 * @author Mathieu
 */
public class Gestionnaire {

    /**
     * Valider et retourne l'identifiant et le mot de passe d'un médecin
     *
     * @param identifiant
     * @param mdp
     * @throws Exception
     */
    public static void authentificationMedecin(String identifiant, String mdp) throws Exception {
        if (ValiderAcces.validerAccesMedecin(identifiant, mdp) == false) {
            throw new Exception("Identifiant/mot de passe invalide");
        }
    }

    /**
     * Valider et retourne l'identifiant et le mot de passe d'un personnel
     * médical
     *
     * @param identifiant
     * @param mdp
     * @throws Exception
     */
    public static void authentificationPM(String identifiant, String mdp) throws Exception {
        if (ValiderAcces.validerAccesPersonnel(identifiant, mdp) == false) {
            throw new Exception("Identifiant/mot de passe invalide");
        }
    }

    /**
     * Obtenir le dossier médical d'un patient.
     *
     * @param numeroAssuranceMaladie
     * @return
     * @throws Exception
     */
    public static DossierMedicalAffichable obtenirDossier(int numeroAssuranceMaladie) throws Exception {
        return new DossierMedicalAdapter(numeroAssuranceMaladie);
        
    }
    
    

    /**
     * Modification d'un dossier patient
     *
     * @param dm
     * @throws Exception
     */
    public static void modifierDossier(DossierMedical dm) throws Exception {
        //Validation
        dm.getPatient().ValiderDonnees();
        for (Antecedent antecedent : dm.getAntecedents()) {
            antecedent.ValiderDonnees();
        }
        for (Visite visite : dm.getVisites()) {
            visite.ValiderDonnees();
        }

        //Enregistrement des données
        Modification mod = new Modification(dm.getId(),
                dm.getPatient().getPrenom(),
                dm.getPatient().getNom(),
                new java.sql.Date(dm.getPatient().getDateNaissance().getYear(), dm.getPatient().getDateNaissance().getMonth(), dm.getPatient().getDateNaissance().getDay()),
                dm.getPatient().getGenre(),
                dm.getPatient().getParentsConnus(),
                dm.getPatient().getVilleNaissance(),
                dm.getPatient().getCoordonnees().getAdresse(),
                dm.getPatient().getCoordonnees().getNumeroTelephone(),
                dm.getPatient().getCoordonnees().getAdresseCourriel(),
                dm.getPatient().getNumeroAssuranceMaladie());
        mod.execute(dm.getId());

        //Antécédents
        for (Antecedent antecedent : dm.getAntecedents()) {
            mod = new Modification(antecedent.getId(),
                    antecedent.getDiagnostic().toString(),
                    antecedent.getTraitement().toString(),
                    antecedent.getIdMedecin(),
                    new java.sql.Date(antecedent.getDebutMaladie().getYear(), antecedent.getDebutMaladie().getMonth(), antecedent.getDebutMaladie().getDay()),
                    new java.sql.Date(antecedent.getFinMaladie().getYear(), antecedent.getFinMaladie().getMonth(), antecedent.getFinMaladie().getDay()));
            mod.execute(antecedent.getId());

        }

        //Visites
        for (Visite visite : dm.getVisites()) {
            mod = new Modification(visite.getId(),
                    visite.getIdMedecin(),
                    visite.getIdEtablissement(),
                    new java.sql.Date(visite.getDate().getYear(), visite.getDate().getMonth(), visite.getDate().getDay()),
                    visite.getDiagnostic().toString(),
                    visite.getTraitement().toString(),
                    visite.getResume(),
                    visite.getNotes());
            mod.execute(visite.getId());
        }
    }

    /**
     * Annuler une modification
     *
     * @param idModification
     * @throws Exception
     */
    public static void annulerModification(int idModification) throws Exception {
        Modification mod = new Modification();
        mod.unexecute(idModification);

    }

    /**
     *
     * @param numeroAssuranceMaladie
     * @return
     * @throws Exception
     */
    public static ArrayList<Integer> obtenirModifcation(int numeroAssuranceMaladie) throws Exception {
        String donnees = ObtenirDossier.obtenirModifications(numeroAssuranceMaladie);
        StringTokenizer st = new StringTokenizer(donnees, "$");
        ArrayList<Integer> retour = new ArrayList<>();

        while (st.hasMoreElements()) {
            retour.add(Integer.parseInt(st.nextElement().toString()));

        }

        return retour;
    }
    

}
