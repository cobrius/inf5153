package API_GESTIONNAIRE.Domaine;

/**
 * Détails d'un professionnel de la sante
 *
 * @author Mathieu
 */
interface ProfessionnelSante {

    public String getId();

    public String getNom();

    public String getPrenom();
}
