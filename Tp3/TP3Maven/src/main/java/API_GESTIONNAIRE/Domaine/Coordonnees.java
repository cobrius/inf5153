package API_GESTIONNAIRE.Domaine;

/**
 * Coordonnées d'un patient
 *
 * @author Mathieu
 */
public class Coordonnees {

    private final String adresse;
    private final String numeroTelephone;
    private final String adresseCourriel;

    /**
     * Builter
     */
    public static class Builder {

        private String adresse = "";
        private String numeroTelephone = "";
        private String adresseCourriel = "";

        public Builder() {
        }

        public void setAdresse(String adresse) {
            this.adresse = adresse;
        }

        public String getAdresse() {
            return this.adresse;
        }

        public void setNumeroTelephone(String numeroTelephone) {
            this.numeroTelephone = numeroTelephone;
        }

        public String getMumeroTelephone() {
            return this.numeroTelephone;
        }

        public void setAdresseCourriel(String adresseCourriel) {
            this.adresseCourriel = adresseCourriel;
        }

        public String getAdresseCourriel() {
            return this.adresseCourriel;
        }

        public Coordonnees getResult() {
            return new Coordonnees(this);
        }

    }

    public Coordonnees(Builder builder) {
        this.adresse = builder.adresse;
        this.numeroTelephone = builder.numeroTelephone;
        this.adresseCourriel = builder.adresseCourriel;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public String getNumeroTelephone() {
        return this.numeroTelephone;
    }

    public String getAdresseCourriel() {
        return this.adresseCourriel;
    }

    /**
     * Valider les coordonnées d'un patient
     *
     * @throws Exception
     */
    public void ValiderDonnees() throws Exception {
        if (this.adresse.equals("")) {
            throw new Exception("Vous devez saisir l'adresse du patient.");
        }
    }

    @Override
    public String toString() {
        return "adresse=" + this.adresse
                + ", numeroTelephone=" + this.numeroTelephone
                + ", adresseCourriel=" + this.adresseCourriel;
    }

}
