package API_GESTIONNAIRE.Domaine;

import java.util.Date;

/**
 * Détails d'un antécédent médical
 *
 * @author Mathieu
 */
public class Antecedent {

    private final int id;
    private final int idPrecedant;
    private final String idMedecin;
    private final Date debutMaladie;
    private final Date finMaladie;
    private final Diagnostic diagnostic;
    private final Traitement traitement;

    /**
     * Builder de la classe antécédent
     */
    public static class AntecedentBuilder {

        private int id = -1;
        private int idPrecedant = -1;
        private String idMedecin = "";
        private Date debutMaladie = null;
        private Date finMaladie = null;
        private Diagnostic diagnostic = null;
        private Traitement traitement = null;

        public AntecedentBuilder() {
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setIdPrecedant(int idPrecedant) {
            this.idPrecedant = idPrecedant;
        }

        public void setIdMedecin(String idMedecin) {
            this.idMedecin = idMedecin;
        }

        public void setDebutMaladie(Date debutMaladie) {
            this.debutMaladie = debutMaladie;
        }

        public void setFinMaladie(Date finMaladie) {
            this.finMaladie = finMaladie;
        }

        public void setDiagnostic(Diagnostic diagnostic) {
            this.diagnostic = diagnostic;
        }

        public void setTraitement(Traitement traitement) {
            this.traitement = traitement;
        }

        public Antecedent getResult() {
            return new Antecedent(this);
        }
    }

    public Antecedent(AntecedentBuilder builder) {
        this.id = builder.id;
        this.idPrecedant = builder.idPrecedant;
        this.idMedecin = builder.idMedecin;
        this.debutMaladie = builder.debutMaladie;
        this.finMaladie = builder.finMaladie;
        this.diagnostic = builder.diagnostic;
        this.traitement = builder.traitement;
    }

    public int getId() {
        return this.id;
    }

    public String getIdMedecin() {
        return this.idMedecin;
    }

    public Date getDebutMaladie() {
        return this.debutMaladie;
    }

    public Date getFinMaladie() {
        return this.finMaladie;
    }

    public Traitement getTraitement() {
        return this.traitement;
    }

    public Diagnostic getDiagnostic() {
        return this.diagnostic;
    }

    /**
     * Valider un antécédent
     *
     * @throws Exception
     */
    public void ValiderDonnees() throws Exception {
        if (this.idMedecin.equals("")) {
            throw new Exception("Vous devez saisir l'identifiant du médecin.");
        } else if (this.debutMaladie == null) {
            throw new Exception("Vous devez saisir la date de début de l'antécédent.");
        } else if (this.finMaladie == null) {
            throw new Exception("Vous devez saisir la date de fin de l'antécédent.");
        } else if (this.diagnostic.toString().equals("")) {
            throw new Exception("Vous devez saisir le diagnostic du patient.");
        }
    }

    @Override
    public String toString() {
        return "id=" + this.id
                + ", idPrecedant=" + this.idPrecedant
                + ", idMedecin=" + this.idMedecin
                + ", debutMaladie" + this.debutMaladie
                + ", finMaladie=" + this.finMaladie
                + ", traitements=" + this.traitement.toString()
                + ", diagnostics=" + this.diagnostic.toString();
    }
}
