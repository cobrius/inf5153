package API_RAMQ.Fondation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Charles-Olivier
 */
public class ConnectionBD {

    public static String BD_DossierMedicaux = "jdbc:sqlite:dossierMedicaux.sqlite";
    public static String BD_Authentification = "jdbc:sqlite:authentification.sqlite";

    public static Connection connecterBD(String adresse) {
        Connection connexion = null;
        try {
            connexion = DriverManager.getConnection(adresse);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return connexion;
    }

    public static Statement creerStatement(Connection connexion) {
        Statement statement = null;
        try {
            statement = connexion.createStatement();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return statement;
    }

    public static ResultSet executerRequete(Statement statement, String requete) {
        ResultSet resultat = null;
        try {
            resultat = statement.executeQuery(requete);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return resultat;
    }

}
