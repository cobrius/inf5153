package API_RAMQ.ServicesTechniques;

import java.sql.Date;

/**
 *
 * @author Charles-Olivier
 */
public class AjoutVisite extends Modification {

    private int id;
    private int idPrecedant;
    private String idMedecin;
    private String etablissement;
    private Date date;
    private String diagnostic;
    private String traitement;
    private String resume;
    private String notes;

    public AjoutVisite(int idPrecedant, String idMedecin, String etablissement, Date date, String diagnostic, String traitement, String resume, String notes) {
        this.idPrecedant = idPrecedant;
        this.idMedecin = idMedecin;
        this.etablissement = etablissement;
        this.date = date;
        this.diagnostic = diagnostic;
        this.traitement = traitement;
        this.resume = resume;
        this.notes = notes;
    }

    private void enregistrerVisite() {
        String requete = "INSERT INTO visite (idPrecedant, idMedecin, etablissement, date, diagnostic, traitement, resume, notes) VALUES (" + this.idPrecedant + ",\"" + this.idMedecin + "\",\"" + this.diagnostic + "\", " + this.date + ",\"" + this.diagnostic + "\", \"" + this.traitement + "\", \"" + this.resume + "\", \"" + this.notes + "\");";
        utils.enregistrer(requete);
    }

    private String getIdVisite() {
        String requete = "SELECT * FROM visite WHERE idPrecedant=\"" + this.idPrecedant + "\";";
        return utils.getId(requete);
    }

    public void updateDossierVisite(String idVisite, int id) {
        String requete = "UPDATE dossier SET idVisite=" + idVisite + " WHERE id=" + id + ";";
        utils.updateDossier(requete);
    }

    @Override
    public void execute(int id) {
        AjoutModification.ajouterModification(id);
        this.idPrecedant = utils.getPrecedant(id, "visite");
        enregistrerVisite();
        String idVisite = getIdVisite();
        updateDossierVisite(idVisite, id);

    }
}
