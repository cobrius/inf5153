package API_RAMQ.ServicesTechniques;

import API_RAMQ.Fondation.ConnectionBD;
import static API_RAMQ.Fondation.ConnectionBD.*;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;

/**
 *
 * @author Charles-Olivier
 */
public class utils {
    protected static int getPrecedant(int id, String type){
        String bd = ConnectionBD.BD_DossierMedicaux;
        String requete = "SELECT * FROM dossier WHERE id='" + id + "';"; 
         Connection connexion = connecterBD(bd);
         Statement statement = creerStatement(connexion);
         ResultSet resultat = executerRequete(statement, requete);
         
         String idPrec = "";
         
         try {
             while (resultat.next()){
                 if(type.equals("visite")){
                    idPrec = resultat.getString("idVisite");                 
                 }else if (type.equals("antecedent")){
                    idPrec = resultat.getString("idAntecedent");
                 }
             }
         } catch ( SQLException e){
             System.out.println(e.getMessage());
         } finally {
            try{ if (resultat != null){ resultat.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (statement != null){ statement.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (connexion != null){ connexion.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
         }
        
        return Integer.parseInt(idPrec);
    }
    
    protected static void enregistrer(String requete){
        String bd = ConnectionBD.BD_DossierMedicaux;
         Connection connexion = connecterBD(bd);
         Statement statement = creerStatement(connexion);
         ResultSet resultat = executerRequete(statement, requete);
         try{ if (resultat != null){ resultat.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
         try{ if (statement != null){ statement.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
         try{ if (connexion != null){ connexion.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};         
    }
    
    protected static String getId(String requete){
        Connection connexion = connecterBD(ConnectionBD.BD_DossierMedicaux);
        Statement statement = creerStatement(connexion);
        ResultSet resultat = executerRequete(statement, requete);
         String id = "";
          try {
             while (resultat.next()){
                 id = resultat.getString("id");                 
             }
         } catch ( SQLException e){
             System.out.println(e.getMessage());
         } finally {
            try{ if (resultat != null){ resultat.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (statement != null){ statement.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (connexion != null){ connexion.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
         }
        
        return id;
    }
    
    protected static void updateDossier(String requete){
        Connection connexion = connecterBD(ConnectionBD.BD_DossierMedicaux);
        Statement statement = creerStatement(connexion);
        ResultSet resultat = executerRequete(statement, requete);
         try{ if (resultat != null){ resultat.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
         try{ if (statement != null){ statement.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
         try{ if (connexion != null){ connexion.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
    }
    
    protected static int getLastModif(int id){
        String requete = "SELECT * FROM modification WHERE idModif=" + id + ";";
        Connection connexion = connecterBD(ConnectionBD.BD_DossierMedicaux);
        Statement statement = creerStatement(connexion);
        ResultSet resultat = executerRequete(statement, requete);
        int idPrecedant = 0;
          try {
             while (resultat.next()){
                 idPrecedant = Integer.parseInt(resultat.getString("idPrecedant"));                 
             }
         } catch ( SQLException e){
             System.out.println(e.getMessage());
         } finally {
            try{ if (resultat != null){ resultat.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (statement != null){ statement.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (connexion != null){ connexion.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
         }     
        return idPrecedant;
    }
    
    protected static void setDossierAsLastModif(int lastModif){
        String req = "SELECT * FROM modification WHERE idModif=" + lastModif + ";";
        Connection connexion = connecterBD(ConnectionBD.BD_DossierMedicaux);
        Statement statement = creerStatement(connexion);
        ResultSet resultat = executerRequete(statement, req);
        
        String requete = "";
        String idPrecedant;
        String idDossier;
        String prenom;
        String nom;
        Date dateNaissance;
        int genre;
        String parents;
        String villeNaissance;
        String adresse;
        String telephone;
        String courriel;
        int noAssuranceMaladie;
        int idAntecedent;
        int idVisite;
        
          try {
                String nouvelIdMod = resultat.getString("idModif");
                while (resultat.next()){
                    idDossier = resultat.getString("idDossier");
                    prenom = resultat.getString("prenom");
                    nom = resultat.getString("nom");
                    StringTokenizer st = new StringTokenizer(resultat.getString("dateNaiss"), "-");
                    dateNaissance = new Date(Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()));
                    genre = Integer.parseInt(resultat.getString("genre"));
                    parents = resultat.getString("parents");
                    villeNaissance = resultat.getString("villeNaiss");
                    adresse = resultat.getString("adresse");
                    telephone = resultat.getString("telephone");
                    courriel = resultat.getString("courriel");
                    noAssuranceMaladie = Integer.parseInt(resultat.getString("noAssMal"));
                    idAntecedent = Integer.parseInt(resultat.getString("idAntecedent"));
                    idVisite = Integer.parseInt(resultat.getString("idVisite"));
                    
                    requete = "UPDATE dossier SET prenom=\"" + prenom + "\", nom=\"" + nom + "\", dateNaiss=\"" + dateNaissance + "\", genre=\"" + genre + "\", parents=\"" + parents + "\", villeNaiss=\"" + villeNaissance + "\", adresse=\"" + adresse + "\", telephone=\"" + telephone + "\", courriel=\"" + courriel + "\", noAssMal=\"" + noAssuranceMaladie + "\", idMod=\"" + nouvelIdMod + "\", idAntecedent=\"" + idAntecedent + "\", idVisite=\"" + idVisite + "\"  WHERE id='" + idDossier + "';";
                }
             
         } catch ( SQLException e){
             System.out.println(e.getMessage());
         } finally {
            try{ if (resultat != null){ resultat.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (statement != null){ statement.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (connexion != null){ connexion.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
         }        
        
        connexion = connecterBD(ConnectionBD.BD_DossierMedicaux);
        statement = creerStatement(connexion);
        resultat = executerRequete(statement, requete);
        try{ if (resultat != null){ resultat.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
        try{ if (statement != null){ statement.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
        try{ if (connexion != null){ connexion.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());}      

          
    }
    
    protected static int getIdModActuel(int idModADetruire){
        String requete = "SELECT * FROM modification WHERE idModif=" + idModADetruire + ";";
        Connection connexion = connecterBD(ConnectionBD.BD_DossierMedicaux);
        Statement statement = creerStatement(connexion);
        ResultSet resultat = executerRequete(statement, requete);
        int idDossier = 0;
        try {
             while (resultat.next()){
                 idDossier = Integer.parseInt(resultat.getString("idDossier"));    
             }
         } catch ( SQLException e){
             System.out.println(e.getMessage());
         } finally {
            try{ if (resultat != null){ resultat.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (statement != null){ statement.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (connexion != null){ connexion.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
         }  
        
        requete = "SELECT * FROM dossier WHERE id=" + idDossier + ";";
        connexion = connecterBD(ConnectionBD.BD_DossierMedicaux);
        statement = creerStatement(connexion);
        resultat = executerRequete(statement, requete);
        int idModActuel = 0;
        try {
             while (resultat.next()){
                 idModActuel = Integer.parseInt(resultat.getString("idMod")); 
             }
         } catch ( SQLException e){
             System.out.println(e.getMessage());
         } finally {
            try{ if (resultat != null){ resultat.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (statement != null){ statement.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (connexion != null){ connexion.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
         }  
        return idModActuel;
    }
    
    
    protected static void effacerRangee(int idModActuel, int idModADetruire){
        if (idModActuel != idModADetruire){
            int idModPrecedant = 0;
            String requete = "SELECT * FROM modification WHERE idModif=" + idModActuel + ";";
            Connection connexion = connecterBD(ConnectionBD.BD_DossierMedicaux);
            Statement statement = creerStatement(connexion);
            ResultSet resultat = executerRequete(statement, requete);
            try {
                 while (resultat.next()){
                     idModPrecedant = Integer.parseInt(resultat.getString("idPrecedant")); 
                 }
             } catch ( SQLException e){
                 System.out.println(e.getMessage());
             } finally {
                try{ if (resultat != null){ resultat.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
                try{ if (statement != null){ statement.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
                try{ if (connexion != null){ connexion.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
             }  
            
            requete = "DELETE FROM modification WHERE idModif=" + idModActuel + ";";
            connexion = connecterBD(ConnectionBD.BD_DossierMedicaux);
            statement = creerStatement(connexion);
            resultat = executerRequete(statement, requete);
            try{ if (resultat != null){ resultat.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (statement != null){ statement.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (connexion != null){ connexion.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());}
            
            effacerRangee(idModPrecedant, idModADetruire);
            
        } else {
            String requete = "DELETE FROM modification WHERE idModif=" + idModADetruire + ";";
            Connection connexion = connecterBD(ConnectionBD.BD_DossierMedicaux);
            Statement statement = creerStatement(connexion);
            ResultSet resultat = executerRequete(statement, requete);
            try{ if (resultat != null){ resultat.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (statement != null){ statement.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());};
            try{ if (connexion != null){ connexion.close(); } }catch (SQLException e ){ System.out.println(e.getMessage());} 
            
        }
    }
    
    
}
