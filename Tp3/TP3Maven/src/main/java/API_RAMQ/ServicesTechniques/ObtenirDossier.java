package API_RAMQ.ServicesTechniques;

import API_RAMQ.Fondation.ConnectionBD;
import static API_RAMQ.Fondation.ConnectionBD.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Charles-Olivier & Mathieu
 */
public class ObtenirDossier {

    private static String bd = ConnectionBD.BD_DossierMedicaux;
    private static Connection connexion;
    private static Statement statement;

    private static ResultSet getResultat(String bd, String requete) {
        connexion = connecterBD(bd);
        statement = creerStatement(connexion);
        ResultSet resultat = executerRequete(statement, requete);
        return resultat;
    }

    private static void fermerConnexion() {
        if (connexion != null) {
            try {
                connexion.close();
            } catch (SQLException e) {
                e.getMessage();
            }
        }
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.getMessage();
            }
        }

    }

    private static String getAntecedent(String idAntecedent) {
        String retour = "";
        while (!idAntecedent.equals("0")) {
            String antecedent;
            ResultSet resultatAnte = getResultat(bd, "SELECT * FROM antecedent WHERE id='" + idAntecedent + "';");
            try {
                while (resultatAnte.next()) {
                    resultatAnte = executerRequete(statement, "SELECT * FROM antecedent WHERE id='" + idAntecedent + "';");
                    antecedent = "";
                    antecedent += "antecedent*" + resultatAnte.getString("id") + "*";
                    String precedant = resultatAnte.getString("idPrecedant");
                    antecedent += "precedent*" + precedant + "*";
                    antecedent += "diagnostic*" + resultatAnte.getString("diagnostic") + "*";
                    antecedent += "traitement*" + resultatAnte.getString("traitement") + "*";
                    antecedent += "idMedecin*" + resultatAnte.getString("idMedecin") + "*";
                    antecedent += "debutMaladie*" + resultatAnte.getString("debutMaladie") + "*";
                    antecedent += "finMaladie*" + resultatAnte.getString("finMaladie") + "*";
                    idAntecedent = precedant;
                    retour += antecedent + "$";
                    try {
                        if (resultatAnte != null) {
                            resultatAnte.close();
                            fermerConnexion();
                        }
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        retour = retour.substring(0, retour.length() - 1);
        return retour;
    }

    private static String getVisite(String idVisite) {
        String retour = "";
        while (!idVisite.equals("0")) {
            String visite;
            ResultSet resultatVis = getResultat(bd, "SELECT * FROM visite WHERE id='" + idVisite + "';");
            try {
                while (resultatVis.next()) {
                    resultatVis = executerRequete(statement, "SELECT * FROM visite WHERE id='" + idVisite + "';");
                    visite = "";
                    visite += "visite/" + resultatVis.getString("id") + "/";
                    String precedant = resultatVis.getString("idPrecedant");
                    visite += "precedent/" + precedant + "/";
                    visite += "idMedecin/" + resultatVis.getString("idMedecin") + "/";
                    visite += "etablissement/" + resultatVis.getString("etablissement") + "/";
                    visite += "date/" + resultatVis.getString("date") + "/";
                    visite += "diagnostic/" + resultatVis.getString("diagnostic") + "/";
                    visite += "traitement/" + resultatVis.getString("traitement") + "/";
                    visite += "resume/" + resultatVis.getString("resume") + "/";
                    visite += "notes/" + resultatVis.getString("notes") + "/";
                    idVisite = precedant;
                    retour += visite + "%";
                    try {
                        if (resultatVis != null) {
                            resultatVis.close();
                            fermerConnexion();
                        }
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        retour = retour.substring(0, retour.length() - 1);
        return retour;
    }

    public static String obtenirDossier(int noAssMal) {
        String requete = "SELECT * FROM dossier WHERE noAssMal='" + noAssMal + "';";
        String retour = "";
        String idAntecedent = "";
        String idVisite = "";

        ResultSet resultat = getResultat(bd, requete);
        try {
            while (resultat.next()) {
                retour = retour + "id$" + resultat.getString("id") + "$";
                retour = retour + "prenom$" + resultat.getString("prenom") + "$";
                retour = retour + "nom$" + resultat.getString("nom") + "$";
                retour = retour + "dateNaiss$" + resultat.getString("dateNaiss") + "$";
                retour = retour + "genre$" + resultat.getString("genre") + "$";
                retour = retour + "parents$" + resultat.getString("parents") + "$";
                retour = retour + "villeNaiss$" + resultat.getString("villeNaiss") + "$";
                retour = retour + "adresse$" + resultat.getString("adresse") + "$";
                retour = retour + "telephone$" + resultat.getString("telephone") + "$";
                retour = retour + "courriel$" + resultat.getString("courriel") + "$";
                retour = retour + "noAssMal$" + resultat.getString("noAssMal") + "$";

                idAntecedent = resultat.getString("idAntecedent");
                idVisite = resultat.getString("idVisite");

                retour = retour + getAntecedent(idAntecedent) + "$";
                retour = retour + getVisite(idVisite) + "%";
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (resultat != null) {
                try {
                    resultat.close();
                } catch (SQLException e) {
                    e.getMessage();
                }
            }
            fermerConnexion();
        }

        return retour;
    }

    /**
     *
     * @param noAssMal
     * @return
     */
    public static String obtenirModifications(int noAssMal) {
        String requete = "SELECT idModif FROM modification WHERE noAssMal='" + noAssMal + "';";
        String retour = "";

        ResultSet resultat = getResultat(bd, requete);
        try {
            while (resultat.next()) {
                retour += resultat.getString("idModif") + "$";
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (resultat != null) {
                try {
                    resultat.close();
                } catch (SQLException e) {
                    e.getMessage();
                }
            }
            fermerConnexion();
        }

        return retour;
    }
}
