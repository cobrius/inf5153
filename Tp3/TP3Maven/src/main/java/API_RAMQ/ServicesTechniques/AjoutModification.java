/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package API_RAMQ.ServicesTechniques;

import API_RAMQ.Fondation.ConnectionBD;
import static API_RAMQ.Fondation.ConnectionBD.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Charles-Olivier
 */
public class AjoutModification {

    public static void ajouterModification(int id) {
        Connection connexion = connecterBD(ConnectionBD.BD_DossierMedicaux);
        Statement statement = creerStatement(connexion);
        String req = "SELECT * FROM dossier WHERE id=" + id + ";";
        ResultSet resultat = executerRequete(statement, req);

        int id_tmp = 0;
        String prenom_tmp = "";
        String nom_tmp = "";
        String date_tmp = "";
        int genre_tmp = 0;
        String parents_tmp = "";
        String ville_tmp = "";
        String adresse_tmp = "";
        String telephone = "";
        String courriel = "";
        int noAss_tmp = 0;
        int idA_tmp = 0;
        int idV_tmp = 0;
        int idM_tmp = 0;

        try {
            while (resultat.next()) {
                id_tmp = Integer.parseInt(resultat.getString("id"));
                prenom_tmp = resultat.getString("prenom");
                nom_tmp = resultat.getString("nom");
                date_tmp = resultat.getString("dateNaiss");
                genre_tmp = Integer.parseInt(resultat.getString("genre"));
                parents_tmp = resultat.getString("parents");
                ville_tmp = resultat.getString("villeNaiss");
                adresse_tmp = resultat.getString("adresse");
                telephone = resultat.getString("telephone");
                courriel = resultat.getString("courriel");
                noAss_tmp = Integer.parseInt(resultat.getString("noAssMal"));
                idA_tmp = Integer.parseInt(resultat.getString("idAntecedent"));
                idV_tmp = Integer.parseInt(resultat.getString("idVisite"));
                idM_tmp = Integer.parseInt(resultat.getString("idMod"));

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (resultat != null) {
                    resultat.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            };
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            };
            try {
                if (connexion != null) {
                    connexion.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            };
        }

        req = "INSERT INTO modification (idPrecedant, idDossier, prenom, nom, dateNaiss, genre, parents, villeNaiss, adresse, telephone, courriel, noAssMal, idAntecedent, idVisite) VALUES (" + idM_tmp + ", " + id + ", \"" + prenom_tmp + "\", \"" + nom_tmp + "\", \"" + date_tmp + "\", " + genre_tmp + ", \"" + parents_tmp + "\", \"" + ville_tmp + "\", \"" + adresse_tmp + "\", \"" + telephone + "\", \"" + courriel + "\", " + noAss_tmp + ", " + idA_tmp + ", " + idV_tmp + ");";
        utils.enregistrer(req);

        connexion = connecterBD(ConnectionBD.BD_DossierMedicaux);
        statement = creerStatement(connexion);
        req = "SELECT * FROM modification WHERE idDossier=" + id_tmp + " AND idPrecedant=" + idM_tmp + ";";
        resultat = executerRequete(statement, req);

        try {
            while (resultat.next()) {
                idM_tmp = Integer.parseInt(resultat.getString("idModif"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (resultat != null) {
                    resultat.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            };
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            };
            try {
                if (connexion != null) {
                    connexion.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            };
        }

        req = "UPDATE dossier SET idMod=" + idM_tmp + " WHERE id=" + id_tmp + ";";
        utils.enregistrer(req);

    }
}
