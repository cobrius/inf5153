package API_RAMQ.ServicesTechniques;

import java.sql.Date;

/**
 *
 * @author Charles-Olivier
 */
public class Modification {

    Modification mod;

    public Modification() {
    }

    public Modification(int id, String prenom, String nom, Date dateNaissance, int genre, String parents, String villeNaissance, String adresse, String telephone, String courriel, int noAssuranceMaladie) {
        mod = new AjoutCoordonnees(id, prenom, nom, dateNaissance, genre, parents, villeNaissance, adresse, telephone, courriel, noAssuranceMaladie);
    }

    public Modification(int idPrecedant, String idMedecin, String etablissement, Date date, String diagnostic, String traitement, String resume, String note) {
        mod = new AjoutVisite(idPrecedant, idMedecin, etablissement, date, diagnostic, traitement, resume, note);
    }

    public Modification(int idPrecedant, String diagnostic, String traitement, String idMedecin, Date debutMaladie, Date finMaladie) {
        mod = new AjoutAntecedent(idPrecedant, diagnostic, traitement, idMedecin, debutMaladie, finMaladie);
    }

    public void execute(int id) {
        mod.execute(id);
    }

    public void unexecute(int id) {
        int lastModif = utils.getLastModif(id);
        int idModActuel = utils.getIdModActuel(id);
        utils.setDossierAsLastModif(lastModif);
        utils.effacerRangee(idModActuel, id);
    }
}
