/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package API_RAMQ.ServicesTechniques;

import java.sql.Date;

/**
 *
 * @author Charles-Olivier
 */
public class AjoutAntecedent extends Modification {

    private int id;
    private int idPrecedant;
    private String diagnostic;
    private String traitement;
    private String idMedecin;
    private Date debutMaladie;
    private Date finMaladie;

    public AjoutAntecedent() {

    }

    public AjoutAntecedent(int idPrecedant, String diagnostic, String traitement, String idMedecin, Date debutMaladie, Date finMaladie) {
        this.idPrecedant = idPrecedant;
        this.diagnostic = diagnostic;
        this.traitement = traitement;
        this.idMedecin = idMedecin;
        this.debutMaladie = debutMaladie;
        this.finMaladie = finMaladie;
    }

    private void enregistrerAntecedent() {
        String requete = "INSERT INTO antecedent (idPrecedant, diagnostic, traitement , idMedecin, debutMaladie, finMaladie) VALUES (" + this.idPrecedant + ",\"" + this.diagnostic + "\", \"" + this.traitement + "\", \"" + this.idMedecin + "\", \"" + this.debutMaladie + "\", \"" + this.finMaladie + "\");";
        utils.enregistrer(requete);
    }

    private String getIdAntecedent() {
        String requete = "SELECT * FROM antecedent WHERE idPrecedant=\"" + this.idPrecedant + "\" AND diagnostic=\"" + this.diagnostic + "\" AND traitement=\"" + this.traitement + "\" AND idMedecin=\"" + this.idMedecin + "\";";
        return utils.getId(requete);
    }

    public void updateDossierAntecedent(String idAntecedent, int id) {
        String requete = "UPDATE dossier SET idAntecedent=" + idAntecedent + " WHERE id=" + id + ";";
        utils.updateDossier(requete);
    }

    @Override
    public void execute(int id) {
        AjoutModification.ajouterModification(id);
        this.idPrecedant = utils.getPrecedant(id, "antecedent");
        enregistrerAntecedent();
        String idAntecedent = getIdAntecedent();
        updateDossierAntecedent(idAntecedent, id);
    }

}
