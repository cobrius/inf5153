package API_RAMQ.ServicesTechniques;

import API_RAMQ.Fondation.ConnectionBD;
import static API_RAMQ.Fondation.ConnectionBD.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Charles-Olivier & Mathieu
 */
public class ValiderAcces {

    /**
     *
     * @param identifiant
     * @param password
     * @return
     * @throws Exception
     */
    public static boolean validerAccesMedecin(String identifiant, String password) throws Exception {
        return validerAcces("authentificationMedecin", identifiant, password);
    }

    /**
     *
     * @param identifiant
     * @param password
     * @return
     * @throws Exception
     */
    public static boolean validerAccesPersonnel(String identifiant, String password) throws Exception {
        return validerAcces("authentificationPersonnel", identifiant, password);
    }

    /**
     *
     * @param requete
     * @param identifiant
     * @param password
     * @return
     * @throws Exception
     */
    private static boolean validerAcces(String table, String identifiant, String password) throws Exception {
        boolean retour = false;
        Connection connexion = connecterBD(ConnectionBD.BD_Authentification);
        Statement statement = creerStatement(connexion);
        ResultSet resultat = executerRequete(statement, "SELECT * FROM " + table + " WHERE id='" + identifiant + "';");

        while (resultat.next()) {
            String pass = resultat.getString("password");
            if (pass.equals(password)) {
                retour = true;
            }
        }

        return retour;
    }

//    /**
//     * INUTILE POUR LE TP3
//     * @param identifiant
//     * @param password
//     * @return
//     * @throws Exception 
//     */
//    public static boolean validerAccesPatient(String identifiant, String password) throws Exception {
//        return validerAcces("authentificationPatient", identifiant, password);
//    }
}
