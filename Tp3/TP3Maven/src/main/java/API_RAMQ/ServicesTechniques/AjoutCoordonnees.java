/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package API_RAMQ.ServicesTechniques;

import java.sql.Date;

/**
 *
 * @author Charles-Olivier
 */
public class AjoutCoordonnees extends Modification {

    private int id;
    private String prenom;
    private String nom;
    private Date dateNaissance;
    private int genre;
    private String parents;
    private String villeNaissance;
    private String adresse;
    private String telephone;
    private String courriel;
    private int noAssuranceMaladie;

    public AjoutCoordonnees(int id, String prenom, String nom, Date dateNaissance, int genre, String parents, String villeNaissance, String adresse, String telephone, String courriel, int noAssuranceMaladie) {
        this.id = id;
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        this.genre = genre;
        this.parents = parents;
        this.villeNaissance = villeNaissance;
        this.adresse = adresse;
        this.telephone = telephone;
        this.courriel = courriel;
        this.noAssuranceMaladie = noAssuranceMaladie;
    }

    @Override
    public void execute(int id) {
        AjoutModification.ajouterModification(this.id);
        String requete = "UPDATE dossier SET prenom=\"" + this.prenom + "\", nom=\"" + this.nom + "\", dateNaiss=\"" + this.dateNaissance + "\", genre=\"" + this.genre + "\", parents=\"" + this.parents + "\", villeNaiss=\"" + this.villeNaissance + "\", adresse=\"" + this.adresse + "\", telephone=\"" + this.telephone + "\", courriel=\"" + this.courriel + "\", noAssMal=\"" + this.noAssuranceMaladie + "\" WHERE id='" + id + "';";
        utils.enregistrer(requete);

    }
}
