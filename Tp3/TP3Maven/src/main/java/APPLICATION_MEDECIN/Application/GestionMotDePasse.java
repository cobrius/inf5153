/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package APPLICATION_MEDECIN.Application;

/**
 *
 * @author cedric
 */
public class GestionMotDePasse {
    char[] motDePasse;

    public GestionMotDePasse(char[] motDePasse) {
        this.motDePasse = motDePasse;
    }

    public char[] getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(char[] motDePasse) {
        this.motDePasse = motDePasse;
    }

    @Override
    public String toString() {
        String retour= "";
       for(int i = 0; i < motDePasse.length; i++){
         retour += motDePasse[i];
       }
       return retour;
    }
    
 
    
}
