---
output:
  pdf_document: default
  html_document: default
---


## Authentification du médecin

### **Périmètre** : Système de gestion de dossiers médicaux

### **Niveau** : But utilisateur

### **Acteur principal** : Médecin

### **Parties prenantes et intérêts**
    * Médecin: Le médecin veut s'authentifier afin d'utilisé le logiciel installé sur un poste de travail ou un ordinateur portable.

### **Préconditions**
    * Le médecin a accès à un poste de travail ou un ordinateur portable possédant le logiciel intallé.
    * Le médecin a reçu son nom d'utilisateur et son mot de passe de la RAMQ.
    * Le logiciel est fonctionnel et opérationnel.

### **Garanties en cas de succès**
    * L'authentification est complètée.
    * Le médecin est redirigé vers la section principale du logiciel.

### **Cas nominal**
    1. Le médecin ouvre le logiciel installé sur son poste de travail ou son ordinateur portable.
    2. Le médecin inscrit son nom d'utilisateur et son mot de passe dans les champs prévus à cet effet et appuie sur le bouton "Poursuivre".
    3. Le médecin est redirigé vers la page principale du logiciel.

### **Extensions**
Aucune

### **Spécification particulières**
    * Une fenêtre indique au médecin que la combinaison du mot de passe et du nom d'utilisateur est erronée.

### **Liste de variantes des données et des technologies**
Aucune

### **Fréquence d'occurence**
Chaque fois que le médecin ouvre le logiciel.

### **Divers**
Aucun


## Authentification du patient (application mobile)

### **Périmètre** : Système de gestion de dossiers médicaux.

### **Niveau** : But utilisateur

### **Acteur principal** : Patient

### **Parties prenantes et intérêts**
    * Patient: Le patient souhaite s'authentifier afin d'utiliser l'application mobile installé sur son téléphone (IOS ou Android).

### **Préconditions**
    * Le patient a accès à un téléphone ayant l'application mobile installée.
    * Le patient a reçu son mot de passe et son nom d'utilisateur de la RAMQ.
    * L'application mobile est fonctionnelle et opérationnelle.

### **Garanties en cas de succès**
    * L'authentification est complètée.
    * Le patient est redirigé vers la page principale de l'application mobile.

### **Cas nominal**
    1. Le patient ouvre l'application mobile installée sur son téléphone.
    2. Le patient entre son nom d'utilisateur et son mot de passe et appuie sur "Poursuivre".
    3. Le patient est redirigé vers la page principale de l'application mobile.

### **Extensions**
Aucune 

### **Spécification particulières**
    * Une fenêtre indique au patient que la combinaison du mot de passe et du nom d'utilisateur est erronée.

### **Liste de variantes des données et des technologies**
Aucune

### **Fréquence d'occurence**
Chaque fois que le patient ouvre l'application.

### **Divers**
Aucun

## Authentification du patient (portail web)

### **Périmètre** : Système de gestion de dossiers médicaux

### **Niveau** : But utilisateur

### **Acteur principal** : Patient

### **Parties prenantes et intérêts**
    * Patient: Le patient souhaite s'authentifier afin d'utiliser le portail web.

### **Préconditions**
    * Le patient a accès à un appareil ayant une connexion internet et un navigateur web..
    * Le patient a reçu son mot de passe et son nom d'utilisateur de la RAMQ.
    * Le portail est fonctionnel et opérationnel.

### **Garanties en cas de succès**
    * L'authentification est complètée.
    * Le patient est redirigé vers la page principale du portail web.

### **Cas nominal**
    1. Le patient ouvre son navigateur web et entre l'adresse du site.
    2. Le patient entre son nom d'utilisateur et son mot de passe et appuie sur "Poursuivre".
    3. Le patient est redirigé vers la page principale du portail web.

### **Extensions**
Aucune 

### **Spécification particulières**
    * Une fenêtre indique au patient que la combinaison du mot de passe et du nom d'utilisateur est erronée.

### **Liste de variantes des données et des technologies**
Aucune

### **Fréquence d'occurence**
Chaque fois que le patient ouvre le portail web.

### **Divers**
Aucun

## Médecin consulte un dossier

### **Périmètre** : Système de gestion de dossiers médicaux

### **Niveau** : But utilisateur

### **Acteur principal** : Médecin

### **Parties prenantes et intérêts**
    * Médecin: Il souhaite consulter le dossier d'un patient
    * Patient: Il souhaite que le médecin puisse consulter son dossier.

### **Préconditions**
    * Le médecin a le logiciel d'installé à son poste de travail ou sur son ordinateur portable.
    * Le médecin a passé l'authentification.

### **Garanties en cas de succès**
    * Le dossier du patient est affiché à l'écran pour la consultation.

### **Cas nominal**
    1. Le médecin appuie sur l'onglet de consultation de dossiers de patients sur le logiciel.
    2. Le médecin identifie le patient à l'aide de sa carte d'assurance maladie.
    3. Le système effectue une requête de consultation et affiche le résultat à l'écran.

### **Extensions**
Aucune

### **Spécification particulières**
    * Le médecin ne peut pas consulter le dossier du médecin s'il n'a pas sa carte d'assurance maladie.
    
### **Liste de variantes des données et des technologies**
Aucune

### **Fréquence d'occurence**
Chaque fois que le médecin recoit la visite d'un patient.

### **Divers**
Aucun


## Médecin ajoute une modification au dossier du patient

### **Périmètre** : Système de gestion de dossiers médicaux

### **Niveau** : But utilisateur

### **Acteur principal** : Médecin

### **Parties prenantes et intérêts**
    * Médecin: Le médecin souhaite inscrire une modification au dossier du patient.
    * Patient: Le patient souhaite avoir son dossier mis à jour.

### **Préconditions**
    * Le médecin a le logiciel installé sur son poste de travail ou son ordinateur portable.
    * Le médecin a passé l'authentification.
    * Le médecin a consulter le dossier du patient.
    * Le médecin a consulter le patient avant d'ajouter la modification.

### **Garanties en cas de succès**
    * La modification a été effectuée.
    * Le médecin est redirigé vers la page de consultation du dossier du patient.

### **Cas nominal**
    1. Le médecin se trouve sur la page de consultation de dossier du patient.
    2. Il ajoute une modification.
    3. Le logiciel formule et envoie une requête de modification de dossier.

### **Extensions**
Aucune

### **Spécification particulières**
    * Le médecin ne confirme pas les modifications. Le logiciel doit envoyer les modifications lorsque le médecin ferme le dossier.

### **Liste de variantes des données et des technologies**
    * Les modifications effectuées par le médecin sont enregistrées dans la base de données de la RAMQ.

### **Fréquence d'occurence**
Au moins une fois chaque fois que le patient visite le médecin.

### **Divers**
Aucun

## Médecin annnule une modification

### **Périmètre** : Système de gestion de dossiers médicaux

### **Niveau** : But utilisateur

### **Acteur principal** : Médecin

### **Parties prenantes et intérêts**
    * Médecin: Il souhaite annuler une modification qu'il a ajoutée au dossier du patient.
    * Patient: Il souhaite que son dossier soit mis à jour.

### **Préconditions**
    *	Le médecin a le logiciel d’installé sur son poste de travail ou son ordinateur portable.
    *	Le médecin a passé l’authentification.
    *	Le médecin se trouve à la section principale du logiciel.

### **Garanties en cas de succès**
    *	La modification a été enlevée.
    *	Le médecin a reçu un message de confirmation.
    *	Le médecin est redirigé vers la section principale.

### **Cas nominal**
    1. Le médecin se trouve sur la page principale du logiciel. 
    2. Il appuie sur l'onglet d'annulation de modification.
    3. Il sélectionne la modification qu'il souhaite annuler.
    4. Le système émet une requête d'annulation de modification et l'envoie à la RAMQ.

### **Extensions**
AUcune

### **Spécification particulières**
    * Le médecin ne peut pas annuler une modification si elle a été faite par un autre médecin.

### **Liste de variantes des données et des technologies**
    * La modification que le médecin souhaitait enlever a été enlevée de la base de données de la RAMQ.

### **Fréquence d'occurence**
À l'occasion, selon la volonté du médecin.

### **Divers**
Aucun

## Plusieurs médecins ajoutent une modification au dossier du patient

### **Périmètre** : Système de gestion de dossiers médicaux

### **Niveau** : But utilisateur

### **Acteur principal** : Médecin-1, Médecin-2

### **Parties prenantes et intérêts**
    * Médecin-1: Il souhaite ajouter une modification au dossier du patient.
    * Médecin-2: Il souhaite ajouter une modification au dossier du patient.
    * Patient: Il souhaite que son dossier soit mis à jour.

### **Préconditions**
    *	Les deux médecins ont le logiciel d’installé sur leur poste de travail ou leur ordinateur portable respectif.
    *	Les deux médecins ont passé l’authentification.
    *	Les deux médecins ont consulté le dossier du patient.
    *	Les deux médecins ont consulté le patient avant de faire les modifications.

### **Garanties en cas de succès**
    *	Les modifications ont été apportées.
    *	La requête reçue la plus tardivement par le gestionnaire de dossiers écrasera les autres en cas de collisions.
    *	Les deux médecins reçoivent des messages de confirmation.
    *	Les deux médecins sont redirigés vers la section principale du logiciel.

### **Cas nominal**
    1. Les deux médecins se trouvent dans la section principale du logiciel.
    2. Les deux médecins clique sur l'onglet d'ajout de modification et y remplissent leur modification.
    3. Le logiciel formulera une requête pour chacune des modifications faites par les médecins.
    4. Le gestionnaire formulera une requête de modification à la RAMQ dans l'ordre qu'il recevera les modifications des médecins. La modification la plus tardive écrasera l'autre en cas de collision.

### **Extensions**
Aucune

### **Spécification particulières**
Aucune

### **Liste de variantes des données et des technologies**
    * Les modifications des deux médecins sont enregistrées dans la base de données de la RAMQ.

### **Fréquence d'occurence**
Rarement.

### **Divers**
Aucun

## Patient consulte son dossier (application mobile)

### **Périmètre** : Système de gestion de dossier médicaux

### **Niveau** : But utilisateur

### **Acteur principal** : Patient

### **Parties prenantes et intérêts**
    * Patient: Le patient souhaite consulter son dossier via l'application mobile.

### **Préconditions**
    * Le patient a téléchargé l'application sur son appareil mobile.
    * Le patient a reçu son nom d'utilsateur et son mot de passe de la RAMQ.
    * Le patient a passé l'authentification.
    * Le patient se trouve sur la page principale de l'application.

### **Garanties en cas de succès**
    * Le dossier du patient est affiché à l'écran pour la consultation.

### **Cas nominal**
    1. Le patient se trouve sur la page principale de l'application mobile.
    2. Il appuie sur l'onglet de consultation de dossier.
    3. Le système effectue une requête de consultation de dossier et le résultat est affiché à l'écran.

### **Extensions**
Aucune

### **Spécification particulières**
Aucune

### **Liste de variantes des données et des technologies**
Aucune

### **Fréquence d'occurence**
Plusieurs fois par jour. À chaque fois qu'un patient souhaite consulter son dossier via l'application mobile.

### **Divers**
Aucun


## Patient consulte son dossier (portail web)

### **Périmètre** : Système de gestion de dossier médicaux

### **Niveau** : But utilisateur

### **Acteur principal** : Patient

### **Parties prenantes et intérêts**
    * Patient: Le patient souhaite consulter son dossier via le portail web.

### **Préconditions**
    * Le patient possède un appareil connecté à internet et possèdant un navigateur web.
    * Le patient a reçu son nom d'utilsateur et son mot de passe de la RAMQ.
    * Le patient a passé l'authentification.
    * Le patient se trouve sur la page principale du portail.

### **Garanties en cas de succès**
    * Le dossier du patient est affiché à l'écran pour la consultation.

### **Cas nominal**
    1. Le patient se trouve sur la page principale du portail web.
    2. Il appuie sur l'onglet de consultation de dossier.
    3. Le système effectue une requête de consultation de dossier et le résultat est affiché à l'écran.

### **Extensions**
Aucune

### **Spécification particulières**
Aucune

### **Liste de variantes des données et des technologies**
Aucune

### **Fréquence d'occurence**
Plusieurs fois par jour. À chaque fois qu'un patient souhaite consulter son dossier via le portail web.

### **Divers**
Aucun


## Patient modifie ses coordonnées (application mobile)

### **Périmètre** : Système de gestion de dossiers médicaux

### **Niveau** : But utilisateur

### **Acteur principal** : Patient

### **Parties prenantes et intérêts**
    * Patient: Le patient souhaite modifier les coordonnées liées à son dossier.

### **Préconditions**
    * Le patient a téléchargé l'application sur son appareil mobile.
    * Le patient a reçu son nom d'utilisateur et son mot de passe de la RAMQ.
    * Le patient a passé l'authentification.
    * Le patient se trouve dans la section principale de l'application mobile.

### **Garanties en cas de succès**
    * Les coordonnées ont été modifiées dans la base de données de la RAMQ.
    * Le patient est redirigé vers la section principale de l'application.

### **Cas nominal**
    1. Le patient se trouve dans la section principale de l'application mobile.
    2. Il appuie sur l'onglet de modification de coordonnées et y modifie le champs souhaités.
    3. Il appuie sur le bouton de confirmation.
    4. Le système envoie une requête de  modification de données.

### **Extensions**
AUcune

### **Spécification particulières**
Aucune

### **Liste de variantes des données et des technologies**
    * Les coordonnées modifiées par le patient sont enregistrées dans la base de données de la RAMQ.
    
### **Fréquence d'occurence**
Occasionnellement, chaque fois qu'un patient souhaite modifier ses coordonnées via l'application mobile.

### **Divers**
Aucun


## Patient modifie ses coordonnées (portail web)

### **Périmètre** : Système de gestion de dossiers médicaux

### **Niveau** : But utilisateur

### **Acteur principal** : Patient

### **Parties prenantes et intérêts**
    * Patient: Le patient souhaite modifier les coordonnées liées à son dossier.

### **Préconditions**
    * Le patient possède un appareil connecté à internet et qui a accès à un navigateur web.
    * Le patient a reçu son nom d'utilisateur et son mot de passe de la RAMQ.
    * Le patient a passé l'authentification.
    * Le patient se trouve dans la section principale du portail web.

### **Garanties en cas de succès**
    * Les coordonnées ont été modifiées dans la base de données de la RAMQ.
    * Le patient est redirigé vers la section principale du portail.

### **Cas nominal**
    1. Le patient se trouve dans la section principale du portail web.
    2. Il appuie sur l'onglet de modification de coordonnées et y modifie le champs souhaités.
    3. Il appuie sur le bouton de confirmation.
    4. Le système envoie une requête de  modification de données.

### **Extensions**
AUcune

### **Spécification particulières**
Aucune

### **Liste de variantes des données et des technologies**
    * Les coordonnées modifiées par le patient sont enregistrées dans la base de données de la RAMQ.
    
### **Fréquence d'occurence**
Occasionnellement, chaque fois qu'un patient souhaite modifier ses coordonnées via le portail web.

### **Divers**
Aucun




















