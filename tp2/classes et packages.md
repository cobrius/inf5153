## Diagrammes de classes et de packages
### API Serveur du gestionnaire
On retrouve dans ce diagramme le package "Domaine". Celui-ci contient tous les objets du domaine ainsi que la logique applicative. 

 **Gestionnaire** 
 - modificationCoordonnees() :todo
 - modificationDossier() :todo
 - obtenirDossier() :todo
 - authentification() :todo
 - reconstruireDossier() :todo
 - annulerModification() :todo

**DossierMedical**
 - On retrouve dans un dossier médical un patient, ses visites ainsi que ses antécédents

**Patient**
 - Contient les informations d'un patient ainsi que ses coordonnées

**Coordonnees**
 - Les coordonnées d'un patient

**Antecedent**
 - Le détail d'un antécédent médical

**Visite**
 - Le détail d'une visite chez le médecin

**Medecin**
 - Les informations sur un médecin (hérité de la classe ProfessionnelSante)

**ProfessionnelSante**
 - Les informations sur un professionnel de la santé

De plus, on y retrouve un sous-package "Modification".

### API Serveur RAMQ
todo
### Application médecin
todo
### Site web et les applications mobiles (iOS et Android)
Le site web et les deux applications mobiles du dossier patient ont les mêmes responsabilités. Bien que les trois applications seront développées dans des langages de programmation différents,  leurs diagrammes de classes et de packages seront identiques. 
	
Premièrement le package "Présentation" contient les classes qui seront utilisées pour générer les interfaces utilisateurs. On y retrouve deux classes :
**InterfaceDossierPatient**

 - afficherPatient() : Affiche les informations du patient (nom, prénom, date de naissance, genre, parents connus, ville de naissance et son numéro d'assurance maladie).
 - afficherVisite() : Affiche la liste des visites faites par le patient (l'établissement visité, le médecin vu, date de la visite, le diagnostic, le traitement et le résumé de la visite).
 - afficherAntecedent() : Affiche la liste des antécédents médical du patient (diagnostic, traitement, le médecin ainsi que la date de début et de fin de la maladie).
 - formulaireCoordonnees() : Affiche un formulaire qui permet au patient de modifier ses coordonnées (adresse de la résidence, le numéro de téléphone et son courriel).  
 
**InterfaceAuthentification**
 - formulaireAuthentification() : Affiche un formulaire où le patient doit saisir un numéro d’assurance maladie et son mot de passe.

Finalement, dans le deuxième package "Application" on retrouvera les classes qui ont comme responsabilités la communication entre l'interface utilisateur et l'API Serveur du gestionnaire.
**DossierPatient**
 - patient() : Récupère les informations du patient de l'API et les transferts à l'interface (afficherPatient()).
 - visite() : Récupère la liste des visites d'un patient de l'API et les transferts à l'interface (afficherVisite()).
 - antecedent() : Récupère la liste des antécédents d'un patient de l'API et les transferts à l'interface (afficherAntecedent()).
 - coordonnees() : Récupère les coordonnées d'un patient de l'API et les transferts à l'interface (formulaireCoordonnees()).
 - enregistrerCoordonnees() : Récupère les coordonnées du formulaire (formulaireCoordonnees()) et les transferts à l'API.

**Authentification**
 - authentifier(): Récupère les coordonnées du formulaire (formulaireAuthentification()) et les transferts à l'API pour valider l'identifiant du patient.